import unittest
import xml_analyser.xml_analyser as xml_analyser
from mock import patch, Mock
from freezegun import freeze_time

mock_date = "2001-01-01 00:00:00+00:00"
mock_date_iso = "2001-01-01T00:00:00+00:00"
mock_date_later = "2002-01-01 00:00:00+00:00"
mock_date_before = "2000-01-01 00:00:00+00:00"


class XMLAnalyserTest(unittest.TestCase):

    def setUp(self) -> None:
        self.app = xml_analyser.Analyse()

    @freeze_time("2001-01-01")
    def test_set_analyse_date(self):
        self.app.set_analyse_date()
        self.assertEqual(self.app.results['analyseDate'], mock_date_iso)

    def test_post_date_check(self):
        self.assertIsNone(self.app.first_post)
        self.assertIsNone(self.app.last_post)
        self.app.post_date_check(mock_date)
        self.assertEqual(str(self.app.first_post), mock_date)
        self.assertEqual(str(self.app.last_post), mock_date)
        self.app.post_date_check(mock_date_later)
        self.assertEqual(str(self.app.last_post), mock_date_later)
        self.assertEqual(str(self.app.first_post), mock_date)
        self.app.post_date_check(mock_date_before)
        self.assertEqual(str(self.app.first_post), mock_date_before)
        self.assertEqual(str(self.app.last_post), mock_date_later)

    @patch.object(xml_analyser.Analyse, 'post_date_check')
    def test_manage_data_from_xml_element(self, mock_post_date_check):
        attrib_dict = {'CreationDate': mock_date,
                       'Score': '1',
                       'AcceptedAnswerId': 'FakeId'}
        test_element = Mock()
        test_element.attrib = attrib_dict
        self.app.manage_data_from_xml_element(test_element)
        mock_post_date_check.assert_called_once_with(mock_date)
        self.assertEqual(self.app.scores_all, 1)
        self.assertEqual(self.app.total_posts, 1)
        self.assertEqual(self.app.total_accepted_posts, 1)

    def test_create_results_details(self):
        mocked_results = {'firstPost': mock_date,
                          'lastPost': mock_date,
                          'avgScore': 1,
                          'totalPosts': 1,
                          'totalAcceptedPosts': 1}
        self.app.scores_all = 1
        self.app.total_accepted_posts = 1
        self.app.total_posts = 1
        self.app.first_post = mock_date
        self.app.last_post = mock_date
        self.app.create_results_details()
        self.assertEqual(self.app.results['details'], mocked_results)

    @patch.object(xml_analyser.Analyse, 'manage_data_from_xml_element')
    @patch.object(xml_analyser.Analyse, 'create_results_details')
    def test_analyse_xml(self, mock_create_details, mock_manage):
        element = Mock()
        element.tag = "row"
        with patch('xml_analyser.xml_analyser.ET.iterparse', return_value=[('event', element)]):
            self.app.local_filename = "fake_file"
            self.app.analyse_xml()
        mock_create_details.assert_called_once()
        mock_manage.assert_called_once_with(element)

    def test_xml_extension_check(self):
        self.assertFalse(self.app.xml_extension_check("http://fake-url.pl/test.html"))
        self.assertTrue(self.app.xml_extension_check("http://fake-url.pl/test.xml"))

    @patch.object(xml_analyser.Analyse, 'analyse_xml')
    @patch('xml_analyser.xml_analyser.reqparse.RequestParser')
    @patch.object(xml_analyser.Analyse, 'download_xml_file')
    @patch.object(xml_analyser.parser, 'parse_args', return_value={'url': 'http://example.org/xml/test.xml'})
    @patch.object(xml_analyser.Analyse, 'set_analyse_date')
    def test_post(self, mock_analyse_date, mock_parse_arg, mock_dl_xml, rq_parse, mock_analyse_xml):
        self.app.post()
        mock_analyse_date.assert_called_once()
        mock_parse_arg.assert_called_once()
        mock_dl_xml.assert_called_once_with('http://example.org/xml/test.xml')
        mock_analyse_xml.assert_called_once()
