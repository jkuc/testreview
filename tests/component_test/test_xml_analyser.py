import json
import os
import pytest
import responses

from xml_analyser import xml_analyser
from freezegun import freeze_time

invalid_url_error = {"ERROR": "Please provide data with valid url"}
invalid_file_error = {"ERROR": "Submitted URL does not point to XML file. Please provide a valid url."}
test_file_valid_response = {"analyseDate": "2001-01-01T00:00:00+00:00",
                            "details": {'firstPost': '2015-07-14 16:39:27.757000+00:00',
                                        'lastPost': '2015-09-14 10:46:52.053000+00:00',
                                        'totalAcceptedPosts': 7,
                                        'avgScore': 2,
                                        'totalPosts': 80}}


@pytest.fixture
def client():
    xml_analyser.app.config['TESTING'] = True
    with xml_analyser.app.test_client() as client:
        yield client


def test_get_empty(client):
    response = client.get('/')
    assert response.status == '404 NOT FOUND'


def test_post_empty(client):
    response = client.post('analyse', data='')
    result = json.loads(response.data.decode())
    assert result == invalid_url_error


def test_post_invalid_url(client):
    response = client.post('analyse', json={"url": "http://fake-url.pl/lol.html"})
    result = json.loads(response.data.decode())
    assert result == invalid_file_error


@freeze_time("2001-01-01")
@responses.activate
def test_small_file(client):
    with open('test_file.xml', 'rb') as test_file:
        responses.add(
            responses.GET, 'http://example.org/xml/test_file.xml',
            body=test_file.read(), status=200,
            stream=True)
        response = client.post('analyse', json={"url": "http://example.org/xml/test_file.xml"})
        assert json.loads(response.data.decode()) == test_file_valid_response
    os.remove('xml_local_file.xml')
