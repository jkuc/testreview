FROM python:alpine3.7

COPY xml_analyser /xml_analyser

COPY requirements.txt /xml_analyser

WORKDIR /xml_analyser

EXPOSE 5000

RUN pip install -r requirements.txt

CMD python xml_analyser.py
