import datetime
import requests
import xml.etree.ElementTree as ET

from flask import Flask
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('url', location='json')

iso_date = datetime.datetime.fromisoformat
timezone_utc = datetime.timezone.utc


class Analyse(Resource):
    def __init__(self):
        self.first_post = None
        self.last_post = None
        self.avg_score = None
        self.results = dict()
        self.local_filename = 'xml_local_file.xml'
        self.total_posts = 0
        self.total_accepted_posts = 0
        self.scores_all = 0

    def download_xml_file(self, url):
        with requests.get(url, stream=True) as r:
            r.raise_for_status()
            with open(self.local_filename, 'wb') as f:
                for chunk in r.iter_content(chunk_size=256):
                    if chunk:
                        f.write(chunk)

    def set_analyse_date(self):
        self.results['analyseDate'] = datetime.datetime.now(timezone_utc).isoformat()

    def post_date_check(self, date):
        date = iso_date(date).astimezone(timezone_utc)
        if not self.first_post or self.first_post > date:
            self.first_post = date
        if not self.last_post or self.last_post < date:
            self.last_post = date

    def manage_data_from_xml_element(self, element):
        creation_date = element.attrib.get('CreationDate')
        self.post_date_check(creation_date)
        self.scores_all += int(element.attrib.get('Score'))
        self.total_posts += 1
        if element.attrib.get('AcceptedAnswerId'):
            self.total_accepted_posts += 1

    def create_results_details(self):
        avg_score = int(self.scores_all / self.total_posts)
        detailed_partial_results = {'firstPost': str(self.first_post),
                                    'lastPost': str(self.last_post),
                                    'avgScore': avg_score,
                                    'totalPosts': self.total_posts,
                                    'totalAcceptedPosts': self.total_accepted_posts}
        self.results['details'] = detailed_partial_results

    def analyse_xml(self):
        for event, element in ET.iterparse(self.local_filename):
            if element.tag == "row":
                self.manage_data_from_xml_element(element)
                element.clear()
        self.create_results_details()

    @staticmethod
    def xml_extension_check(url):
        return url.lower().endswith('.xml')

    def post(self):
        args = parser.parse_args()
        url = args.get('url')
        if not url:
            return {'ERROR': "Please provide data with valid url"}
        if not self.xml_extension_check(url):
            return {"ERROR": "Submitted URL does not point to XML file. Please provide a valid url."}
        self.set_analyse_date()
        self.download_xml_file(url)
        self.analyse_xml()
        return self.results


api.add_resource(Analyse, '/analyse')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
